namespace Kundenklassifizierung
{
    partial class CKlassifizierung
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_input_name = new System.Windows.Forms.TextBox();
            this.rbtn_privatkunde = new System.Windows.Forms.RadioButton();
            this.rbtn_firmenkunde = new System.Windows.Forms.RadioButton();
            this.lbl_output = new System.Windows.Forms.Label();
            this.cmd_view = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_ende = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // txt_input_name
            // 
            this.txt_input_name.Location = new System.Drawing.Point(125, 45);
            this.txt_input_name.Name = "txt_input_name";
            this.txt_input_name.Size = new System.Drawing.Size(100, 22);
            this.txt_input_name.TabIndex = 1;
            // 
            // rbtn_privatkunde
            // 
            this.rbtn_privatkunde.AutoSize = true;
            this.rbtn_privatkunde.Location = new System.Drawing.Point(41, 94);
            this.rbtn_privatkunde.Name = "rbtn_privatkunde";
            this.rbtn_privatkunde.Size = new System.Drawing.Size(97, 20);
            this.rbtn_privatkunde.TabIndex = 2;
            this.rbtn_privatkunde.Text = "Privatkunde";
            this.rbtn_privatkunde.UseVisualStyleBackColor = true;
            // 
            // rbtn_firmenkunde
            // 
            this.rbtn_firmenkunde.AutoSize = true;
            this.rbtn_firmenkunde.Location = new System.Drawing.Point(41, 151);
            this.rbtn_firmenkunde.Name = "rbtn_firmenkunde";
            this.rbtn_firmenkunde.Size = new System.Drawing.Size(104, 20);
            this.rbtn_firmenkunde.TabIndex = 3;
            this.rbtn_firmenkunde.Text = "Firmenkunde";
            this.rbtn_firmenkunde.UseVisualStyleBackColor = true;
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(38, 199);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(0, 16);
            this.lbl_output.TabIndex = 4;
            // 
            // cmd_view
            // 
            this.cmd_view.Location = new System.Drawing.Point(41, 250);
            this.cmd_view.Name = "cmd_view";
            this.cmd_view.Size = new System.Drawing.Size(82, 49);
            this.cmd_view.TabIndex = 2;
            this.cmd_view.Text = "&Anzeige";
            this.cmd_view.UseVisualStyleBackColor = true;
            this.cmd_view.Click += new System.EventHandler(this.cmd_view_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(150, 250);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(93, 49);
            this.cmd_clear.TabIndex = 3;
            this.cmd_clear.Text = "Anzeige &Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_ende
            // 
            this.cmd_ende.Location = new System.Drawing.Point(273, 250);
            this.cmd_ende.Name = "cmd_ende";
            this.cmd_ende.Size = new System.Drawing.Size(79, 49);
            this.cmd_ende.TabIndex = 7;
            this.cmd_ende.TabStop = false;
            this.cmd_ende.Text = "&Beenden";
            this.cmd_ende.UseVisualStyleBackColor = true;
            this.cmd_ende.Click += new System.EventHandler(this.cmd_ende_Click);
            // 
            // CKlassifizierung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 322);
            this.Controls.Add(this.cmd_ende);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_view);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.rbtn_firmenkunde);
            this.Controls.Add(this.rbtn_privatkunde);
            this.Controls.Add(this.txt_input_name);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CKlassifizierung";
            this.Text = "Kundenklassifizierung";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_input_name;
        private System.Windows.Forms.RadioButton rbtn_privatkunde;
        private System.Windows.Forms.RadioButton rbtn_firmenkunde;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.Button cmd_view;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_ende;
    }
}

