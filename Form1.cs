using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kundenklassifizierung
{
    public partial class CKlassifizierung : Form
    {
        public CKlassifizierung()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void cmd_ende_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_view_Click(object sender, EventArgs e)
        {
            // Verarbeitung: welcher Radio-Button wurde aktiviert?
            // Radio-Button Firmenkunde aktiviert?
            // Bedingung ist wahr, wenn die Methode den boolschen Wert True zurückliefert
            if (rbtn_firmenkunde.Checked)
                // Kundenname und Kundenart ausgeben
                lbl_output.Text = txt_input_name.Text + " \nKundenart: Firmenkunde";
            // Radio-Button Privatkunde aktiviert?
            // Bedingung ist wahr, wenn die Methode den boolschen Wert True zurückliefert
            if (rbtn_privatkunde.Checked)
                lbl_output.Text = txt_input_name.Text + " \nKundenart: Privatkunde";
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            // Text- und Label-Eigenschaft: Text löschen
            txt_input_name.Text = lbl_output.Text = null;
            // Rückgabewert der Methode checked auf False setzen
            rbtn_privatkunde.Checked = rbtn_firmenkunde.Checked = false;
            // Focus (Cursor) in die textbox txt_input_name platzieren
            txt_input_name.Focus();
        }
    }
}